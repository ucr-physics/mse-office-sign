# UCR MSE Office Sign

[![The Unlicense](https://img.shields.io/badge/license-The_Unlicense-blue.svg)](./LICENSE.txt)

## Usage

1. Clone or download this.
2. Edit `mse-office-sign.tex`
   according to the comments in that file.
   Simply use the `\person` macro in between
   the begin and end document statements as shown.
3. Build using LaTeX with `latexmk -pdf`.

![MSE Office Sign](https://bitbucket.org/ucr-physics/mse-office-sign/raw/master/mse-office-sign.svg)

## License

This work is released into the public domain.

The UCR Logo and UCR Seal are the property of
The University of California, Riverside.
Permitted usage is detailed on the [Creative Design Services] website.

[Creative Design Services]: https://creativedesign.ucr.edu/standards.html

## Warranty

This work is provided "as is" and without any express or
implied warranties, including, without limitation, the implied
warranties of merchantibility and fitness for a particular
purpose.
